# eerie-sheen

A collection of VSCode themes that eschew the bright tones for a more eerie sheen.

## Themes
### Eerie Sheen Base
![Eerie Sheen screenshot](https://gitlab.com/funkhouser/eerie-sheen/-/raw/main/resources/eerieSheenBase.png)
